// halaman awal
app.get("/", pageindex);

// halaman add
app.get("/add", pageadd);

// halaman update
app.get("/update/:id", pageupdate);

// menambahkan data
app.post("/add", addDT);

// hapus data
app.get("/delete/:id", deleteDT);

// edit data
app.post("/update", updateDT);

// filter data
app.get("/filter/:size", filterDT);

app.use("/", notfound);
